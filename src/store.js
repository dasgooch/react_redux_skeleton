/*
 * src/store.js
 * No initialState
*/
// import { createStore, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';
// import rootReducer from './reducers/RootReducer';
// export default function configureStore() {
//  return createStore(
//   rootReducer,
//    applyMiddleware(thunk)
//  );
// }

/*
 * src/store.js
 * With initialState
*/
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';

//So you can use redux dev tools
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialState={}) {
 return createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(thunk))
 );
}