import React, { Component } from 'react';
import { connect } from 'react-redux';
import { simpleAction } from './actions/simpleAction.js'
import './App.css';

const mapStateToProps = state => ({
  ...state
})
const mapDispatchToProps = dispatch => ({
  simpleAction: () => dispatch(simpleAction())
})
class App extends Component {
  simpleAction = (event) => {
    this.props.simpleAction();
  }
  
  render() {
    return (
      <div className="App">
        <button onClick={this.simpleAction}>Test redux action</button>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);